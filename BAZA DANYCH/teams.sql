-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Czas generowania: 07 Lut 2020, 09:24
-- Wersja serwera: 10.4.10-MariaDB
-- Wersja PHP: 7.4.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `teams`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `league`
--

DROP TABLE IF EXISTS `league`;
CREATE TABLE IF NOT EXISTS `league` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=727 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `league`
--

INSERT INTO `league` (`id`, `name`, `img`, `user_id`) VALUES
(3, 'LaLiga', 'LaLiga.png', 0),
(1, 'Bundesliga', 'Bundesliga.jpg', 0),
(5, 'Premier League', 'PremierLeague.jpg', 0),
(4, 'League 1', 'Ligue1.png', 0),
(6, 'SerieA', 'SerieA.jpg', 0),
(2, 'Ekstraklasa', 'Ekstraklasa.jpg', 0),
(15, 'Liga Admina', 'avatar.jpg', 15);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `match`
--

DROP TABLE IF EXISTS `match`;
CREATE TABLE IF NOT EXISTS `match` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `home_team` int(10) DEFAULT NULL,
  `visitor_team` int(10) DEFAULT NULL,
  `home_score` int(11) DEFAULT NULL,
  `home_team_points` int(11) NOT NULL,
  `visitor_score` int(11) DEFAULT NULL,
  `visitor_team_points` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `match_day_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `match`
--

INSERT INTO `match` (`id`, `home_team`, `visitor_team`, `home_score`, `home_team_points`, `visitor_score`, `visitor_team_points`, `date`, `match_day_id`) VALUES
(21, 142, 143, 0, 1, 0, 1, '2020-02-05', 63),
(20, 43, 52, 0, 1, 0, 1, '2020-01-10', 44),
(19, 43, 52, 0, 1, 0, 1, '2020-01-01', 47);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `match_day`
--

DROP TABLE IF EXISTS `match_day`;
CREATE TABLE IF NOT EXISTS `match_day` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `league_id` int(11) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=64 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `match_day`
--

INSERT INTO `match_day` (`id`, `league_id`, `number`) VALUES
(44, 1, 4),
(43, 1, 3),
(62, 15, 2),
(61, 15, 1),
(63, 15, 6),
(47, 1, 1),
(45, 1, 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `migration`
--

DROP TABLE IF EXISTS `migration`;
CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) COLLATE utf8_polish_ci NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1578398155),
('m130524_201442_init', 1578398157),
('m190124_110200_add_verification_token_column_to_user_table', 1578398157),
('m200122_081251_create_match_table', 1579681406),
('m200122_082237_create_match_day_table', 1579681406);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `team`
--

DROP TABLE IF EXISTS `team`;
CREATE TABLE IF NOT EXISTS `team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `coach` varchar(255) NOT NULL,
  `league_id` int(11) NOT NULL,
  `stadium` varchar(255) NOT NULL,
  `capacity` varchar(255) NOT NULL,
  `town` varchar(255) NOT NULL,
  `value` decimal(12,0) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=144 DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `team`
--

INSERT INTO `team` (`id`, `name`, `coach`, `league_id`, `stadium`, `capacity`, `town`, `value`) VALUES
(1, 'Liverpool F.C', 'Klopp Jurgen', 5, 'Anfield', '54,074', 'Liverpool', '1180'),
(2, 'Leicester City F.C', 'Rodgers Brendan', 5, 'King Power Stadium', '32,312', 'Leicester', '506'),
(3, 'Manchester City F.C', 'Guardiola Pep', 5, 'Etihad Stadium', '55,097', 'Manchester', '1300'),
(4, 'Chealse F.C', 'Lampard Frank', 5, 'Stamford Bridge', '41,631', 'London', '843'),
(5, 'FC Barcelona', 'Valverde Ernesto', 3, 'Camp Nou', '98,812', 'Barcelona', '1180'),
(6, 'Real Madryt C.F', 'Zidane Zinedine', 3, 'Santiago Bernabeu Stadium', '81,044', 'Madryt', '1190'),
(7, 'Tottenham Hotspur F.C.', 'Jose Mourinho', 5, 'Tottenham Hotspur Stadium', '62,062', 'London', '960'),
(8, 'Manchester United F.C.', 'Ole Gunnar Solskjaer', 5, 'Old Trafford', '76,000', 'Manchester', '748'),
(9, 'Sheffield United F.C.', 'Chris Wilder', 5, 'Bramall Lane', '32,125', 'Sheffield', '116'),
(10, 'Wolverhampton Wanderers F.C.', 'Nuno Espirito Santo', 5, 'Molineux Stadium', '31,700', 'Wolverhampton', '358'),
(12, 'Arsenal F.C.', 'Freddie Ljungberg', 5, 'Emirates Stadium', '60,260', 'London', '678'),
(13, 'Newcastle United F.C.', 'Steve Bruce', 5, ' St. James\' Park', '52,405', 'Newcastle', '269'),
(14, 'Burnley F.C.', 'Sean Dyche', 5, 'Turf Moor', '22,546', 'Burnley', '195'),
(15, 'Brighton & Hove Albion Football Club', 'Graham Potter', 5, 'American Express Community Stadium', '30,666', 'Brighton', '217'),
(16, 'A.F.C. Bournemouth', 'Eddie Howe', 5, 'Dean Court', '11,329', 'Bournemouth', '342'),
(17, 'West Ham United F.C.', 'Manuel Pellegrini', 5, 'London Stadium', '66,000', 'Stratford', '335'),
(18, 'Everton F.C.', 'Carlo Ancelotti', 5, 'Goodison Park', '39,572', 'Liverpool', '496'),
(19, 'Aston Villa F.C.', 'Dean Smith', 5, 'Villa Park', '42,785', 'Birmingham', '251'),
(20, 'Southampton F.C.', 'Ralph Hasenhuttl', 5, 'St Mary\'s Stadium', '32,384', 'Southampton', '225'),
(21, 'Norwich City F.C.', 'Daniel Farke', 5, 'Carrow Road', ' 27,244', 'Norwich', '142'),
(22, 'Watford F.C.', 'Nigel Pearson', 3, 'Vicarage Road', '21,577', 'Watford', '208'),
(23, 'Atletico Madrid', 'Diego Simeone', 3, 'Wanda Metropolitano Stadium', '68,456', 'Madryt', '873'),
(24, 'Valencia CF', 'Albert Celades', 3, 'Mestalla Stadium', '55,000', 'Valencia', '496'),
(25, 'Real Sociedad', 'Imanol Alguacil', 3, 'Anoeta Stadium', '39,500', 'San Sebastian', '310'),
(26, 'Real Betis', 'Rubi', 3, 'Benito Villamarin Stadium', '60,721', 'Sevilla', '293'),
(27, 'Sevilla FC', 'Julen Lopetegui', 3, 'Ramon Sanchez-Pizjuan Stadium', '43,883', 'Sevilla', '283'),
(28, 'Celta de Vigo', 'Oscar Garcia Junyent', 3, 'Balaidos', '29,000', 'Vigo', '226'),
(29, 'Athletic Bilbao', 'Gaizka Garitano', 3, 'San Mames Stadium', '53,289', 'Bilbao', '225'),
(30, 'Villarreal CF', 'Javier Calleja', 3, 'Estadio de la Ceramica', '23,500', 'Villareal', '212'),
(31, 'Getafe CF', 'Jose Bordalas', 3, 'Coliseum Alfonso Perez', '17,000', 'Getafe', '153'),
(32, 'RCD Espanyol de Barcelona', 'Pablo Machin', 3, 'RCDE Stadium', '40,000', 'Barcelona', '150'),
(33, 'CD Leganes', 'Javier Aguirre', 3, 'Estadio Municipal de Butarque', '12,450', 'Leganes', '107'),
(34, 'Levante UD', 'Paco Lopez', 3, 'Estadi Ciutat de Valencia', '26,354', 'Valencia', '102'),
(35, 'Real Valladolid', 'Sergio', 3, 'Estadio Jose Zorrilla', '26,512', 'Valladolid', '93'),
(36, 'Deportivo Alaves', 'Asier Garitano', 3, 'Mendizorrotza Stadium', '19,840', 'Vitoria-Gasteiz', '90'),
(37, 'SD Eibar', 'Jose Luis Mendilibar', 3, 'Ipurua Municipal Stadium', '8,164', 'Eibar', '70'),
(38, 'RCD Mallorca', 'Vicente Moreno', 3, 'Iberostar Stadium', '23,142', 'Majorca', '59'),
(39, 'CA Osasuna', 'Jagoba Arrasate', 3, 'El Sadar Stadium', '18,375', 'Pamplona', '44'),
(40, 'Granada CF', 'Diego Martinez', 3, 'Nuevo Estadio de Los Carmenes', '19,336', 'Granada', '34'),
(41, 'LKS Lodz', 'Kazimierz Moskal', 2, 'Stadion LKS', '1,924', 'Lodz', '6'),
(42, 'A.C. Milan', 'Stefano Pioli', 6, 'San Siro Stadium', '80,018', 'Milan', '461'),
(61, 'Rakow Czestochowa', 'Marek Papszun', 2, 'Miejski Stadion Pilkarski Rakow w Czestochowie', '6500', 'Czestochowa', '8'),
(43, 'FC Bayern Munich', 'Hans-Dieter Flick', 1, 'Allianz Arena', '75024', 'Munich', '893'),
(44, 'Borussia Dortmund', 'Lucien Favre', 1, 'Signal Iduna Park', '81365', 'Dortmund', '618'),
(45, 'RB Leipzig', 'Julian Nagelsmann', 1, 'Red Bull Arena', '41939', 'Leipzig', '590'),
(46, 'Bayer 04 Leverkusen', 'Peter Bosz', 1, 'BayArena', '30210', 'Leverkusen', '415'),
(47, 'Borussia Monchengladbach', 'Marco Rose', 1, 'BORUSSIA-PARK', '59724', 'Monchengladbach', '317'),
(48, 'FC Schalke 04', 'David Wagner', 1, 'VELTINS-Arena', '62271', 'Gelsenkirchen', '240'),
(49, 'TSG 1899 Hoffenheim', 'Alfred Schreuder', 1, 'Rhein-Neckar-Arena', '30150', 'Hoffeinhein', '239'),
(50, 'VfL Wolfsburg', 'Oliver Glasner', 1, 'Volkswagen Arena', '30000', 'Wolfsburg', '238'),
(51, 'Eintracht Frankfurt', 'Adi Hutter', 1, 'Commerzbank-Arena', '51500', 'Frankfurt', '220'),
(52, 'Hertha BSC', 'Jurgen Klinsmann', 1, 'Olympiastadion Berlin', '74475', 'Berlin', '217'),
(53, 'SV Werder Bremen', 'Florian Kohfeldt', 1, 'Weser Stadium', '42358', 'Bremen', '170'),
(54, 'SC Freiburg', 'Christian Streich', 1, 'Schwarzwald-Stadion', '24000', 'Freiburg', '149'),
(55, '1. FSV Mainz 05', 'Achim Beierlorzer', 1, 'Opel Arena', '33305', 'Mainz', '145'),
(56, 'FC Augsburg', 'Martin Schmidt', 1, 'Augsburg Arena', '30660', 'Augsburg', '132'),
(57, '1. FC Koln', 'Markus Gisdol', 1, 'RheinEnergieSTADION', '49968', 'Cologne', '88'),
(58, 'Fortuna Dusseldorf', 'Friedhelm Funkel', 1, 'MERKUR SPIEL-ARENA', '54600', 'Dusseldorf', '86'),
(59, '1. FC Union Berlin', 'Urs Fischer', 1, 'Stadion An der Alten Forsterei', '22012', 'Berlin', '40'),
(60, 'SC Paderborn 07', 'Steffen Baumgart', 1, 'Benteler Arena', '15000', 'Paderborn', '32'),
(62, 'Wisla Plock', 'Radoslaw Sobolewski', 2, 'Kazimierz Gorski Stadium', '10978', 'Plock', '9'),
(63, 'Zaglebie Lubin', 'Martin Sevela', 2, 'Stadion Zaglebia Lubin', '16068', 'Lubin', '9'),
(64, 'Slask Wroclaw', 'Vitezslav Lavicka', 2, 'Stadion Miejski we Wroclawiu', '43302', 'Wroclaw', '9'),
(65, 'Arka gdynia', 'Aleksandar Rogic', 2, 'Stadion GOSiR', '15139', 'Gdynia', '9'),
(66, 'Wisla Krakow', 'Piotr Obidzinski', 2, 'Stadion Miejski w Krakowie', '32804', 'Krakow', '9'),
(67, 'Gornik Zabrze', 'Marcin Brosz', 2, 'Ernest Pohl Stadium', '31871', 'Zabrze', '10'),
(68, 'Korona Kielce', 'Mirosław Smyla', 2, 'Kielce City Stadium', '15500', 'Kielce', '10'),
(69, 'Piast Gliwice', 'Waldemar Fornalik', 2, 'Stadion Piast', '9913', 'Gliwice', '10'),
(70, 'KS Cracovia', 'Janusz Filipiak', 2, 'Cracovia Stadium', '14300', 'Krakow', '11'),
(71, 'Pogon Szczecin', 'Kosta Runjaic', 2, 'Stadion Florian Krygier', '18027', 'Szczecin', '13'),
(72, 'Lechia Gdansk', 'Piotr Stokowiec', 2, 'Stadion Energa Gdansk', '43615', 'Gdansk', '14'),
(73, 'Jagiellonia Bialystok', 'Cezary Kulesza', 2, 'Bialystok City Stadium', '22372', 'Bialystok', '15'),
(74, 'Lech Poznan', 'Dariusz Zuraw', 2, 'Poznan Stadium', '43269', 'Poznan', '25'),
(75, 'Legia Warszawa', 'Dariusz Mioduski', 2, 'Marshall Jozef Pilsudski\'s Municipal Stadium of Legia Warsaw', '31103', 'Warszawa', '29'),
(93, 'Juventus F.C.', 'Maurizio Sarri', 6, 'Allianz Stadium', '41507', 'Turin', '810'),
(94, 'Inter Milan', 'Antonio Conte', 6, 'San Siro Stadium', '80.018', 'Mediolan', '656'),
(95, 'S.S.C. Napoli', 'Gennaro Gattuso', 6, 'San Paolo Stadium', '55000', 'Naples', '631'),
(96, 'A.S. Roma', 'Paulo Fonseca', 6, 'Stadio Olimpico', '72.698', 'Rome', '448'),
(97, 'S.S. Lazio', 'Simone Inzaghi', 6, 'Stadio Olimpico', '70634', 'Rome', '331'),
(98, 'Atalanta B.C.', 'Gian Piero Gasperini', 6, 'Stadio Atleti Azzurri d\'Italia', '21300', 'Bergamo', '285'),
(99, 'ACF Fiorentina', 'Vincenzo Montella', 6, 'Artemio Franchi Stadium', '43147', 'Fiorence', '273'),
(100, 'Torino F.C.', 'Walter Mazzarri', 6, 'Olympic Grande Torino Stadium', '28177', 'Turin', '224'),
(101, 'Cagliari Calcio', 'Rolando Maran', 6, 'Sardegna Arena', '16233', 'Cagliari', '191'),
(102, 'U.S. Sassuolo Calcio', 'Roberto De Zerbi', 6, 'Mapei Stadium - Citta del Tricolore', '23717', 'Sassuolo', '175'),
(103, 'U.C. Sampdoria', 'Claudio Ranieri', 6, 'Stadio Luigi Ferraris', '36599', 'Genoa', '149'),
(104, 'Udinese Calcio', 'Luca Gotti', 6, 'Stadio Friuli', '25132', 'Udine', '142'),
(105, 'Genoa C.F.C.', 'Davide Nicola', 6, 'Stadio Luigi Ferraris', '36599', 'Genoa', '139'),
(106, 'Parma Calcio 1913', 'Roberto D\'Aversa', 6, 'Ennio Tardini Stadium', '22352', 'Parma', '113'),
(107, 'Bologna F.C. 1909', 'Sinisa Mihajlović', 6, 'Stadio Renato Dall\'Ara', '38279', 'Bologna', '106'),
(108, 'Brescia Calcio', 'Eugenio Corini', 6, 'Stadio Mario Rigamonti', '16743', 'Brescia', '96'),
(109, 'Hellas Verona F.C.', 'Ivan Jurić', 6, 'Stadio Marc\'Antonio Bentegodi', '39211', 'Verona', '81'),
(110, 'S.P.A.L.', 'Leonardo Semplici', 6, 'Stadio Paolo Mazza', '16134', 'Ferrara', '69'),
(111, 'U.S. Lecce', 'Fabio Liverani', 6, 'Stadio Via del Mare', '31533', 'Lecce', '50'),
(113, 'Paris Saint-Germain F.C.', 'Thomas Tuchel', 4, 'Le Parc des Princes', '47929', 'Paris', '1010'),
(114, 'Olympique Lyonnais', 'Rudi Garcia', 4, 'Groupama Stadium', '59186', 'Lyon', '382'),
(115, 'AS Monaco FC', 'Leonardo Jardim', 4, 'Stade Louis II', '18523', 'Monaco', '330'),
(116, 'Lille OSC', 'Christophe Galtier', 4, 'Stade Pierre Mauroy', '50186', 'Lille', '267'),
(117, 'Olympique de Marseille', 'Andre Villas-Boas', 4, 'Orange Velodrome', '67394', 'Marseille', '247'),
(118, 'OGC Nice', 'Patrick Vieira', 4, 'Allianz Riviera - Nice Stadium', '35624', 'Nice', '190'),
(119, 'Stade Rennais F.C.', 'Julien Stephan', 4, 'Roazhon Park', '29778', 'Rennes', '173'),
(120, 'FC Girondins de Bordeaux', 'Paulo Sousa', 4, 'Matmut Atlantique', '42115', 'Bordeaux', '136'),
(121, 'AS Saint-Etienne', 'Claude Puel', 4, 'Stade Geoffroy-Guichard', '41965', 'Saint-Etienne', '131'),
(122, 'Montpellier HSC', 'Michel Der Zakarian', 4, 'Stade de la Mosson', '32900', 'Montpellier', '98'),
(123, 'RC Strasbourg Alsace', 'Thierry Laurey', 4, 'Stade de la Meinau', '29320', 'Strasbourg', '97'),
(124, 'FC Nantes', 'Christian Gourcuff', 4, 'Stade de la Beaujoire', '37473', 'Nantes', '88'),
(125, 'Stade de Reims', 'David Guion', 4, 'Stade Auguste-Delaune', '21.029', 'Reims', '88'),
(126, 'Toulouse FC', 'Antoine Kombouare', 4, 'Stadium TFC', '33150', 'Toulouse', '86'),
(127, 'Angers SCO', 'Stephane Moulin', 4, 'Stade Raymond Kopa', '18752', 'Angers', '73'),
(128, 'Amiens SC', 'Luka Elsner', 4, 'Stade de la Licorne', '12097', 'Amiens', '63'),
(129, 'FC Metz', 'Vincent Hognon', 4, 'Stade Saint-Symphorien', '25636', 'Metz', '52'),
(130, 'Stade Brestois 29', 'Olivier Dall\'Oglio', 4, 'Stade Francis-Le Ble', '15931', 'Brest', '52'),
(131, 'Dijon FCO', 'Stephane Jobard', 4, 'Stade Gaston Gerard', '15995', 'Dijon', '52'),
(132, 'Nimes Olympique', 'Bernard Blaquart', 4, 'Stade des Costieres', '18482', 'Nimes', '48'),
(141, 'Druzynaroku', 'Waluuu', 11, 'dadasda', '104', 'asdfgh', '8'),
(142, 'qwerty', 'qwerty', 15, 'asdfgh', '10000', 'asdfgh', '12'),
(143, 'asdfgh', 'adada', 15, 'sadsdasd', '2321', 'dsadada', '10');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `surname` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `city` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `favouriteClub` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT 10,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `name`, `surname`, `city`, `favouriteClub`, `status`, `created_at`, `updated_at`, `verification_token`) VALUES
(14, 'Tomasz', '4WsZdDAVZeiLWughvYClXmIPiET4XGnR', '$2y$13$j51v1AHVpE0K..UQ0ioxJuWUOQdK6vP68er..YhJWlX.IrWiE2b6y', NULL, 'tomasz@gmail.com', '', '', '', '', 10, 1580371683, 1580378424, '-N3e5u6rrPK_mq0Il46y0vgGECJWS1mP_1580371683'),
(15, 'Admin', '2eaLCh77gqphmIhtUhr76G-gPNyz3Zcm', '$2y$13$pU6qMm4r5dWoQD1iEMFXDOQ6sZbM1K5JUurNI3.09I3bT97XxLLfe', NULL, 'admin123@gmail.com', 'Admin', 'Admin', 'Admin', '17', 10, 1580455462, 1580992832, '12CtBHxm3BkL5zaHPM6ZcWNJRH3UR0ul_1580455462'),
(16, 'qwerty', 'LnN7JhlwDpM6J460ZnwYLCtuV8mvvGOh', '$2y$13$6JwNrYqhgV1syKDwK1.yOeK6Mh7uN4KIhvS6qI/RmOBXkmKn6kbky', NULL, 'qwerty@gmail.com', NULL, NULL, NULL, NULL, 10, 1580455555, 1580455555, 'zsyFSK4u7W45B9UL2gW5XftcAfiGjqsO_1580455555'),
(17, 'mariusz', 'GSMQgzDZwZ9elxHdqjq1lezARJU_QQ7p', '$2y$13$1x5YJUORHKjKPO7snEmuhu41d/i9dHcu8rS3UBlT3wrIcIagmbDn2', NULL, 'example@example.com', NULL, NULL, NULL, NULL, 10, 1580714968, 1580715059, '40QCUL0UH3KcnQFUhShlgZvslvbSst4x_1580714968'),
(18, 'niedzialahaslo', 'Rt16wgHKWluGfFVKgVaPNJa9WEdsiMYp', '$2y$13$Z7tg/xnNPoLsL79CBSjRKOyGNIrvmJYkqYUsG9t9O0.5Mc.S./6U2', NULL, 'haslo@gmail.com', NULL, NULL, NULL, NULL, 10, 1580989235, 1580989235, 'Uet34f3PUjKTC3omGr6eKXjvW-li4HW3_1580989235');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uzytkownicy`
--

DROP TABLE IF EXISTS `uzytkownicy`;
CREATE TABLE IF NOT EXISTS `uzytkownicy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` text COLLATE utf8_polish_ci NOT NULL,
  `pass` text COLLATE utf8_polish_ci NOT NULL,
  `email` text COLLATE utf8_polish_ci NOT NULL,
  `authKey` char(50) COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `uzytkownicy`
--

INSERT INTO `uzytkownicy` (`id`, `user`, `pass`, `email`, `authKey`) VALUES
(1, 'adam', '$2y$10$WTpf6VZZuWi/w4iEczlrUOBzZLKK5aibAjaDHYjhjXc2gi7kYxOSu', 'adam@gmail.com', ''),
(2, 'marek', '$2y$10$KBJbb6rZ97M81Hu7hxsMLOgKY/uAxeQY1HIVNZ0Z5ByScdOStuhky', 'marek@gmail.com', ''),
(3, 'anna', 'zxcvb', 'anna@gmail.com', ''),
(4, 'andrzej', 'asdfg', 'andrzej@gmail.com', ''),
(5, 'justyna', 'yuiop', 'justyna@gmail.com', ''),
(6, 'kasia', 'hjkkl', 'kasia@gmail.com', ''),
(7, 'beata', 'fgthj', 'beata@gmail.com', ''),
(8, 'jakub', 'ertyu', 'jakub@gmail.com', ''),
(9, 'janusz', 'cvbnm', 'janusz@gmail.com', ''),
(10, 'roman', 'dfghj', 'roman@gmail.com', ''),
(11, 'Admin', '$2y$10$qDqKQTVVtn3ctbvLJ3wAxuugXhNwqS2cn2BwdhJlgYyzwNmvgMDNi', 'admin@gmail.com', '12341234'),
(12, 'Testaccount', '$2y$10$bQZzimStSjwWO2wUbuzED.4/6nBkF8TmAOniHSdZ2rM.S8TRrcO/S', 'example@example.com', ''),
(13, 'mariusz', '$2y$10$zHOdriXD6MsuCZCnXWNGvO7Odeg8zBhFVml/WBwUgBBuIbT/4AdIW', 'example3@example.com', '');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
