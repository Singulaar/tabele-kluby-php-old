<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\VarDumper;

/**
 * MatchSearch represents the model behind the search form of `app\models\Match`.
 */
class MatchSearch extends Match
{
    public $home_team_points;
    public $visitor_team_points;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'home_team', 'visitor_team', 'home_score', 'home_team_points', 'visitor_score', 'visitor_team_points', 'match_day_id'], 'integer'],
            [['date'], 'safe'],
            [['home_team_name', 'visitor_team_name'], 'string']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Match::find()->select([
            'match.visitor_team',
            'match.home_team',
            'match.home_score',
            'match.visitor_score',
            'match.visitor_team_points',
            'match.home_team_points',
            'match.date',
            'match.match_day_id',
            'match.id',
            'match.id',
            'match.id',
            'home_team_name' => 'ht.name',
            'visitor_team_name' => 'vt.name',
        ])->joinWith('homeTeam ht')->joinWith('visitorTeam vt');


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'home_score', $this->home_score])
            ->andFilterWhere(['like', 'home_team_points', $this->home_team_points])
            ->andFilterWhere(['like', 'visitor_score', $this->visitor_score])
            ->andFilterWhere(['like', 'visitor_team_points', $this->visitor_team_points])
            ->andFilterWhere(['like', 'date', $this->date])
            ->andFilterWhere(['like', 'match_day_id', $this->match_day_id])
            ->andFilterWhere(['like', 'ht.name', $this->home_team_name])
            ->andFilterWhere(['like', 'vt.name', $this->visitor_team_points]);

        return $dataProvider;
    }
}
