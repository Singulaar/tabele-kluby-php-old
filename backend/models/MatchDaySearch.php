<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\VarDumper;


/**
 * MatchDaySearch represents the model behind the search form of `app\models\MatchDay`.
 */
class MatchDaySearch extends MatchDay
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'league_id', 'number'], 'integer'],
            [['league_name'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MatchDay::find()->select([
            'match_day.league_id',
            'league_name' => 'league.name',
            'match_day.id',
            'match_day.number',
        ])->leftJoin(League::tableName(), MatchDay::tableName() . '.league_id = ' . League::tableName() . '.id');


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'league_id', $this->league_id])
            ->andFilterWhere(['like', 'number', $this->number])
            ->andFilterWhere(['like', 'league.name', $this->league_name]);

        return $dataProvider;
    }
}
