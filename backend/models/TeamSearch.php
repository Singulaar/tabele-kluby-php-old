<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Team;

/**
 * TeamSearch represents the model behind the search form of `app\models\Team`.
 */
class TeamSearch extends Team
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'league_id'], 'integer'],
            [['name', 'coach', 'stadium', 'capacity', 'town', 'league_name'], 'safe'],
            [['value'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Team::find()->select([
            'team.id',
            'team.name',
            'team.coach',
            'team.league_id',
            'team.stadium',
            'team.capacity',
            'team.town',
            'team.value',
            'league_name' => 'league.name'
        ])->leftJoin(League::tableName(), Team::tableName() . '.league_id= ' . League::tableName() . '.id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'id', $this->id])
            ->andFilterWhere(['like', 'league_id', $this->league_id])
            ->andFilterWhere(['like', 'value', $this->value])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'coach', $this->coach])
            ->andFilterWhere(['like', 'stadium', $this->stadium])
            ->andFilterWhere(['like', 'capacity', $this->capacity])
            ->andFilterWhere(['like', 'league.name', $this->league_name])
            ->andFilterWhere(['like', 'town', $this->town]);


        return $dataProvider;
    }
}
