<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\MatchDaySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Match Days';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="match-day-index">
    <div class="content">
        <aside class="admin-menu-aside">
            <ol>
                <li><h3>MENU</h3></li>
                <li><?= Html::a('Home page', ['site/index']) ?></li>
                <li><?= Html::a('Leagues', ['league/index']) ?></li>
                <li><?= Html::a('Teams', ['team/index']) ?></li>
                <li><?= Html::a('Users', ['user/index']) ?></li>
                <li class="user-menu-open"><?= Html::a('Matchdays', ['match-day/index']) ?></li>
                <li><?= Html::a('Match', ['match/index']) ?></li>
            </ol>
        </aside>
    </div>
    <div class="container admin-menu-container">
        <h1><?= Html::encode($this->title) ?></h1>

        <p>
            <?= Html::a('Create Match Day', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

<!--        --><?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'league_name',
                'number',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>

</div>
