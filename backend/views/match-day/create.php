<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\MatchDay */

$this->title = 'Create Match Day';
$this->params['breadcrumbs'][] = ['label' => 'Match Days', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="match-day-create">
    <div class="content">
        <aside class="admin-menu-aside">
            <ol>
                <li><h3>MENU</h3></li>
                <li><?= Html::a('Home page', ['site/index']) ?></li>
                <li><?= Html::a('Leagues', ['league/index']) ?></li>
                <li><?= Html::a('Teams', ['team/index']) ?></li>
                <li><?= Html::a('Users', ['user/index']) ?></li>
                <li class="user-menu-open"><?= Html::a('Matchdays', ['match-day/index']) ?></li>
                <li><?= Html::a('Match', ['match/index']) ?></li>
            </ol>
        </aside>
    </div>
    <div class="container admin-menu-container">
        <h1><?= Html::encode($this->title) ?></h1>

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
