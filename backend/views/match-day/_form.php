<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MatchDay */
/* @var $form yii\widgets\ActiveForm */
/* @var $leagueList array */
/* @var $numberList array */
?>

<div class="match-day-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'league_name')->dropDownList($leagueList, ['value' => $model->league_id]) ?>

    <?= $form->field($model, 'number')->dropDownList($numberList, ['value' => $model->number]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
