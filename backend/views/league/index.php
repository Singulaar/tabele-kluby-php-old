<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\LeagueSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Leagues';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="league-index">
    <div class="content">
        <aside class="admin-menu-aside">
            <ol>
                <li><h3>MENU</h3></li>
                <li><?= Html::a('Home page', ['site/index']) ?></li>
                <li class="user-menu-open"><?= Html::a('Leagues', ['league/index']) ?></li>
                <li><?= Html::a('Teams', ['team/index']) ?></li>
                <li><?= Html::a('Users', ['user/index']) ?></li>
                <li><?= Html::a('Matchdays', ['match-day/index']) ?></li>
                <li><?= Html::a('Match', ['match/index']) ?></li>
            </ol>
        </aside>
    </div>
    <div class="container admin-menu-container">
        <h1><?= Html::encode($this->title) ?></h1>

        <p>
            <?= Html::a('Create League', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'name',
                'img',
                'user_id',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>

    </div>
</div>
