<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';

use yii\helpers\Html; ?>

<div class="content">
    <aside class="admin-menu-aside">
        <ol>
            <li><h3>MENU</h3></li>
            <li class="user-menu-open">Home page</li>
            <li><?= Html::a('Leagues', ['league/index']) ?></li>
            <li><?= Html::a('Teams', ['team/index']) ?></li>
            <li><?= Html::a('Users', ['user/index']) ?></li>
            <li><?= Html::a('Matchdays', ['match-day/index']) ?></li>
            <li><?= Html::a('Match', ['match/index']) ?></li>
        </ol>
    </aside>
    <div class="container admin-menu-container">
        <h1>Welcome, <?= Yii::$app->user->identity->username ?></h1>
    </div>
</div>
