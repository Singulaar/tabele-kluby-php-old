<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $teamList array */
/* @var $model backend\models\Match */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="match-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'homeTeam')->dropDownList($teamList) ?>

    <?= $form->field($model, 'visitorTeam')->dropDownList($teamList) ?>

    <?= $form->field($model, 'home_score')->textInput() ?>

    <?= $form->field($model, 'visitor_score')->textInput() ?>

    <?= $form->field($model, 'date')->textInput(['type' => 'date']) ?>

    <?= $form->field($model, 'match_day_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
