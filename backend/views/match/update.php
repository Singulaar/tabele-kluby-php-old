<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Match */
/* @var $visitor_Team backend\models\Match */
/* @var $home_Team backend\models\Match */
/* @var $teamList array */

$this->title = 'Update Match: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Matches', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="match-update">
    <div class="content">
        <aside class="admin-menu-aside">
            <ol>
                <li><h3>MENU</h3></li>
                <li><?= Html::a('Home page', ['site/index']) ?></li>
                <li><?= Html::a('Leagues', ['league/index']) ?></li>
                <li><?= Html::a('Teams', ['team/index']) ?></li>
                <li><?= Html::a('Users', ['user/index']) ?></li>
                <li><?= Html::a('Matchdays', ['match-day/index']) ?></li>
                <li class="user-menu-open"><?= Html::a('Match', ['match/index']) ?></li>
            </ol>
        </aside>
    </div>
    <div class="container admin-menu-container">
        <h1><?= Html::encode($this->title) ?></h1>

        <?= $this->render('_form', [
            'model' => $model,
            'teamList' => $teamList,
        ]) ?>
    </div>
</div>
