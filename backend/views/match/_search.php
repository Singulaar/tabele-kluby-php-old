<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MatchSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="match-search">
    <div class="content">
        <aside class="admin-menu-aside">
            <ol>
                <li><h3>MENU</h3></li>
                <li><?= Html::a('Home page', ['site/index']) ?></li>
                <li><?= Html::a('Leagues', ['league/index']) ?></li>
                <li><?= Html::a('Teams', ['team/index']) ?></li>
                <li><?= Html::a('Users', ['user/index']) ?></li>
                <li><?= Html::a('Matchdays', ['match-day/index']) ?></li>
                <li class="user-menu-open"><?= Html::a('Match', ['match/index']) ?></li>
            </ol>
        </aside>
    </div>
    <div class="container admin-menu-container">
        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>

        <?= $form->field($model, 'id') ?>

        <?= $form->field($model, 'home_team') ?>

        <?= $form->field($model, 'visitor_team') ?>

        <?= $form->field($model, 'home_score') ?>

        <?= $form->field($model, 'home_team_points') ?>

        <?php // echo $form->field($model, 'visitor_score') ?>

        <?php // echo $form->field($model, 'visitor_team_points') ?>

        <?php // echo $form->field($model, 'date') ?>

        <?php // echo $form->field($model, 'match_day_id') ?>

        <div class="form-group">
            <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
