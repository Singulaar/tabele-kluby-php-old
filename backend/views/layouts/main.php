<?php

/* @var $this View */

/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
use yii\web\View;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="user-menu-body">
<?php if(!Yii::$app->user->isGuest): ?>
<header class="admin-menu-header">
    <div class="navigation admin-menu-navigation">
        <div class="nav_item"><a href="../../frontend/web/user-menu/menu"
            <p>USER PANEL</p></a></div>
        <div class="nav_item"><?= Html::a('Logout (' . Yii::$app->user->identity->username . ')', Url::to(['site/logout']), ['data-method' => 'POST']) ?></a></div>
    </div>
    <div class="nav-item-logo">FOOTBALL MANAGER</div>
</header>
<?php endif ?>
<?php $this->beginBody() ?>
<?= $content ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
