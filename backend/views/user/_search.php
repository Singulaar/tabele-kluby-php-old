<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\UserSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-search">
    <div class="content">
        <aside class="admin-menu-aside">
            <ol>
                <li class="admin-menu-title"><h3>MENU</h3></li>
                <li><?= Html::a('Home page', ['site/index']) ?></li>
                <li><?= Html::a('Leagues', ['league/index']) ?></li>
                <li><?= Html::a('Teams', ['team/index']) ?></li>
                <li class="user-menu-open"><?= Html::a('Users', ['user/index']) ?></li>
                <li><?= Html::a('Matchdays', ['match-day/index']) ?></li>
                <li><?= Html::a('Match', ['match/index']) ?></li>
            </ol>
        </aside>
    </div>
    <div class="container admin-menu-container">
        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>

        <?= $form->field($model, 'id') ?>

        <?= $form->field($model, 'username') ?>

        <?= $form->field($model, 'auth_key') ?>

        <?= $form->field($model, 'password_hash') ?>

        <?= $form->field($model, 'password_reset_token') ?>

        <?php // echo $form->field($model, 'email') ?>

        <?php // echo $form->field($model, 'name') ?>

        <?php // echo $form->field($model, 'surname') ?>

        <?php // echo $form->field($model, 'city') ?>

        <?php // echo $form->field($model, 'favouriteClub') ?>

        <?php // echo $form->field($model, 'status') ?>

        <?php // echo $form->field($model, 'created_at') ?>

        <?php // echo $form->field($model, 'updated_at') ?>

        <?php // echo $form->field($model, 'verification_token') ?>

        <div class="form-group">
            <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
