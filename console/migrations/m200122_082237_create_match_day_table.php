<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%match_day}}`.
 */
class m200122_082237_create_match_day_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%match_day}}', [
            'id' => $this->primaryKey(),
            'league_id' => $this->integer(),
            'number' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%match_day}}');
    }
}
