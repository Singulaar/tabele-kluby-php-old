<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%match-day}}`.
 */
class m200122_081251_create_match_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%match-day}}', [
            'id' => $this->primaryKey(),
            'home_team' => $this->integer(),
            'visitor_team' => $this->integer(),
            'home_score' => $this->integer(),
            'visitor_score' => $this->integer(),
            'date' => $this->date(),
            'match_day_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%match-day}}');
    }
}
