<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "match_day".
 *
 * @property int $id
 * @property int|null $league_id
 * @property int|null $number
 */
class MatchDay extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match_day';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['league_id', 'number'], 'integer'],
            ['number', 'validateNumber', 'when' => function ($model) {
                return $model->number > 0;
            }
            ],
        ];
    }

    public function validateNumber()
    {
        $num = MatchDay::find()->where(['league_id' => $this->league_id])->andWhere(['number' => $this->number])->exists();
        if ($num) {
            $this->addError('number', 'Matchday is exist.');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'league_id' => 'League ID',
            'number' => 'Number',
        ];
    }
}
