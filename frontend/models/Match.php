<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "match-day".
 *
 * @property int $id
 * @property string|null $home_team
 * @property string|null $visitor_team
 * @property int|null $home_score
 * @property int|null $visitor_score
 * @property string|null $date
 * @property int|null $match_day_id
 *
 * @property Team $homeTeam
 * @property Team $visitorTeam
 */
class Match extends \yii\db\ActiveRecord
{

    public $home_team_name;
    public $home_team_points;
    public $visitor_team_points;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'match';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['home_score', 'visitor_score', 'match_day_id'], 'integer'],
            [['date'], 'safe'],
            [['date'], 'required'],
            [['home_team', 'visitor_team'], 'string', 'max' => 255],
            ['visitor_team', function ($attribute) {
                if ($this->home_team === $this->visitor_team) {
                    $this->addError($attribute, 'Teams cant play with each other.');
                }
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'home_team' => 'Home Team',
            'visitor_team' => 'Visitor Team',
            'home_score' => 'Home Score',
            'visitor_score' => 'Visitor Score',
            'date' => 'Date',
            'match_day_id' => 'Match Day ID',
        ];
    }

    public function getHomeTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'home_team']);
    }

    public function getVisitorTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'visitor_team']);
    }
}
