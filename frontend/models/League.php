<?php

namespace frontend\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * This is the model class for table "league".
 *
 * @property int $id
 * @property string $name
 * @property string $img
 * @property int $user_id
 * @property resource $image
 */
class League extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'league';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'img'], 'required'],
            [['user_id'], 'integer'],
            [['img'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
            [['name'], 'string', 'max' => 255],
            ['user_id', 'validateLeague', 'when' => function ($model) {
                return $model->user_id > 0;
            }
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function validateLeague()
    {
        $id = Yii::$app->user->id;
        $user_league = League::find()->where(['user_id' => $id])->exists();


        if ($user_league) {
            $this->addError('name', 'Your league is exist.');
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'img' => 'Img',
            'user_id' => 'User ID',
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->img->saveAs('uploads/' . $this->img->baseName . '.' . $this->img->extension);
            return true;
        } else {
            return false;
        }
    }
}
