<?php

namespace frontend\models;

use Yii;
use yii\db\ActiveRecord;
use yii\base\Model;

/**
 * This is the model class for table "team".
 *
 * @property int $id
 * @property string $name
 * @property string $coach
 * @property int $league_id
 * @property string $stadium
 * @property string $capacity
 * @property string $town
 * @property float $value
 */
class Team extends ActiveRecord
{

    public $points;
    public $goals_scored;
    public $goals_conceded;
    public $balance;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'team';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'coach', 'stadium', 'capacity', 'town', 'value'], 'required'],
            [['league_id'], 'integer'],
            [['value', 'capacity'], 'number'],
            [['name', 'coach', 'stadium', 'capacity', 'town'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'coach' => 'Coach',
            'league_id' => 'League ID',
            'stadium' => 'Stadium',
            'capacity' => 'Capacity',
            'town' => 'Town',
            'value' => 'Value',
        ];
    }
}


