<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="user-menu-body">
<header class="user-menu-header">
    <div class="navigation user-menu-navigation">
        <div class="nav_item"><a href="<?= Url::to(['/']); ?>"
            <p>HOME</p></a></div>
        <div class="nav_item"><a href="<?= Url::to(['site/about']); ?>"
            <p>ABOUT</p></a></div>
        <div class="nav_item"><a href="<?= Url::to(['site/contact']); ?>"
            <p>CONTACT</p></a></div>
        <div class="nav_item"><?= Html::a('Logout (' . Yii::$app->user->identity->username . ')', Url::to(['site/logout']), ['data-method' => 'POST']) ?></a></div>
    </div>
    <div class="nav-item-logo">FOOTBALL MANAGER</div>
</header>

<?php $this->beginBody() ?>
<?= $content ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
