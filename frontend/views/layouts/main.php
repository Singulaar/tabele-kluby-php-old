<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<header>
    <div class="date">
        <script charset="UTF-8"
                src="https://edodatki.pl/code/zegar-cyfrowy?data%5BWidget%5D%5Btitle%5D=Zegar&data%5BWidget%5D%5Bcss%5D=1"></script>
    </div>
    <br>
    <div class="date date--position"><p>Date: <?php echo date('d.m.Y') ?></p></div>

    <div class="navigation">
        <div class="nav_item"><a href="<?= Url::to(['/']); ?>"
            <p>HOME</p></a></div>
        <div class="nav_item"><a href="<?= Url::to(['site/about']); ?>"
            <p>ABOUT</p></a></div>
        <div class="nav_item"><a href="<?= Url::to(['site/contact']); ?>"
            <p>CONTACT</p></a></div>
        <div class="nav_item"><?= Html::a('Logout (' . Yii::$app->user->identity->username . ')', Url::to(['site/logout']), ['data-method' => 'POST']) ?></a></div>
        <div class="nav_item"><?= Html::a('MENU', ['user-menu/menu']) ?></div>
</header>

<?php $this->beginBody() ?>

<div class="wrap">
    <div class="container">
        <?= $content ?>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
