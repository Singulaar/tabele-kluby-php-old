<?php

/* @var $this yii\web\View */
/* @var $user User */
/* @var $match Team */

/* @var $teamsList array */

use common\models\User;
use frontend\models\Team;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


$this->title = 'My Yii Application - Personal Data';
?>

<div class="content">

    <aside class="user-menu-aside">
        <ol>
            <li class="user-menu-title"><h3>MENU</h3></li>
            <li><?= Html::a('Home page', ['menu']) ?></li>
            <li class="user-menu-open"><?= Html::a('Personal data', ['personal-data']) ?></li>
            <li><?= Html::a('Change password', ['change-password']) ?></li>
            <li><?= Html::a('Change email', ['change-email']) ?></li>
        </ol>
    </aside>

    <div class="row">
        <div class="col-4 offset-4 user-menu-welcome">
            <h1>Welcome, <?= Yii::$app->user->identity->username ?></h1>
            <p>Manage your data, privacy and security.</p>
        </div>
    </div>

    <div class="container user-menu-container">
        <div class="user-menu-content">
            <h2>Update your personal data:</h2>
            <?php $form = ActiveForm::begin(['action' => 'personal-data']) ?>
            <?= $form->field($user, 'newName')->textInput(['value' => "$user->name", 'placeholder' => "Name"])->label(false) ?>
            <?= $form->field($user, 'newSurname')->textInput(['value' => "$user->surname", 'placeholder' => "Surname"])->label(false) ?>
            <?= $form->field($user, 'newCity')->textInput(['value' => "$user->city", 'placeholder' => "City"])->label(false) ?>
            <p>Choose your favourite club:</p>
            <?= $form->field($user, 'newFavouriteClub')->dropDownList($teamsList,['value' => $user->favouriteClub] )->label(false) ?>
            <?= \yii\helpers\Html::submitButton('Update data', ['class' => 'btn btn-primary btn_position']) ?>
            <?php \yii\widgets\ActiveForm::end() ?>
        </div>
    </div>
