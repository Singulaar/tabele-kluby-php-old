<?php

/* @var $this yii\web\View */

/* @var $user User */

use common\models\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'My Yii Application - Change Email';
?>

<div class="content">

    <aside class="user-menu-aside">
        <ol>
            <li class="user-menu-title"><h3>MENU</h3></li>
            <li><?= Html::a('Home page', ['menu']) ?></li>
            <li><?= Html::a('Personal data', ['personal-data']) ?></li>
            <li><?= Html::a('Change password', ['change-password']) ?></li>
            <li class="user-menu-open"><?= Html::a('Change email', ['change-email']) ?></li>
        </ol>
    </aside>

    <div class="row">
        <div class="col-4 offset-4 user-menu-welcome">
            <h1>Welcome, <?= Yii::$app->user->identity->username ?></h1>
            <p>Manage your data, privacy and security.</p>
        </div>
    </div>

    <div class="container user-menu-container">
        <div class="user-menu-content">
            <h2>Change email:</h2>
            <?php $form = ActiveForm::begin() ?>
            <p>Your current email:</p>
            <?= $form->field($user, 'email')->textInput(['placeholder' => "Old email"])->label(false) ?>
            <?= $form->field($user, 'newEmail')->textInput(['placeholder' => "New email"])->label(false) ?>
            <?= \yii\helpers\Html::submitButton('Change email', ['class' => 'btn btn-primary btn_position']) ?>
            <?php \yii\widgets\ActiveForm::end() ?>
        </div>
    </div>
</div>
