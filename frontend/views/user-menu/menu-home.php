<?php

/* @var $this yii\web\View */
/* @var $user User */
/* @var $newFavouriteClub User */

use yii\helpers\Html;
use common\models\User;

$this->title = 'My Yii Application - User Menu';
?>

<div class="content">

    <aside class="user-menu-aside">
        <ol>
            <li class="user-menu-title"><h3>MENU</h3></li>
            <li class="user-menu-open">Home page</li>
            <li><?= Html::a('Personal data', ['personal-data']) ?></li>
            <li><?= Html::a('Change password', ['change-password']) ?></li>
            <li><?= Html::a('Change email', ['change-email']) ?></li>
        </ol>
    </aside>

    <div class="row">
        <div class="col-4 offset-4 user-menu-welcome">
            <h1>Welcome, <?= Yii::$app->user->identity->username ?></h1>
            <p>Manage your data, privacy and security.</p>
        </div>
    </div>

    <div class="container user-menu-container">
        <div class="user-menu-content user-menu-content--image">
            <div class="row"><h3 class="col-4 offset-4">Privacy data:</h3></div>
            <ol>
                <li>Login: <?= Yii::$app->user->identity->username ?></li>
                <li>Email: <?= Yii::$app->user->identity->email ?></li>
                <li>Name: <?= $user->name ?></li>
                <li>Surname: <?= $user->surname ?></li>
                <li>City: <?= $user->city ?></li>
                <li>Favourite Club: <?= $newFavouriteClub -> name ?></li>
            </ol>
        </div>
    </div>
</div>
