<?php

/* @var $this yii\web\View */
/* @var $league League */
/* @var $team Team */
/* @var ActiveDataProvider $dataProvider */

/* @var $match_day_last MatchDay */

use frontend\models\League;
use frontend\models\MatchDay;
use frontend\models\Team;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'My Yii Application - League';
?>

<div class="container">
    <div>
        <a href="<?= Url::to(['/']); ?>"><h3><-- Back</h3></a>
        <h1><?= Html::encode("{$league->name}") ?>: 19/20</h1>
        <?= Html::a('Create a match-day -->', ['match-day/add', 'id' => $league->id], ['class' => 'match-day']) ?>
        <?php if ($match_day_last): ?>
            <?= Html::a('Matchdays', ['match-day/matchdays', 'id' => $league->id, 'match_day_id' => $match_day_last->id], ['class' => 'matches']) ?>
        <?php endif ?>
        <?= Html::a('Live table', ['match-day/table', 'id' => $league->id], ['class' => 'match-day match-day--table']) ?>
        <div class="content">
            <?= \yii\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'class' => 'team_table',
                'columns' => [
                    'name',
                    'coach',
                    'stadium',
                    'capacity',
                    'town',
                    'value'
                ],
                'summary' => false,
            ]) ?>
        </div>
    </div>



