<?php
/* @var $this yii\web\View */
/* @var $league League */
/* @var $team Team */
/* @var $new_team Team */
/* @var $teamList array */
/* @var $delete_team Team */
/* @var ActiveDataProvider $dataProvider */

/* @var $match_day_last MatchDay */

use frontend\models\League;
use frontend\models\MatchDay;
use frontend\models\Team;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'My Yii Application - My Team';
?>

<aside>
    <div class="tools form-group">
        <ul>

            <?php $form = ActiveForm::begin(); ?>
            <li><p class="team_add delete_team_hidden delete_league_hidden">Add team</p>
                <div class="my_team_add">
                    <h5>Add team</h5>
                    <?= $form->field($new_team, 'name')->textInput(['placeholder' => "Team name"])->label(false) ?>

                    <?= $form->field($new_team, 'coach')->textInput(['placeholder' => "Trainer's name"])->label(false) ?>

                    <?= $form->field($new_team, 'stadium')->textInput(['placeholder' => "Stadium"])->label(false) ?>

                    <?= $form->field($new_team, 'capacity')->textInput(['placeholder' => "Capacity", 'type' => 'number', 'max' => 112000, 'min' => 100])->label(false) ?>

                    <?= $form->field($new_team, 'town')->textInput(['placeholder' => "Town"])->label(false) ?>

                    <?= $form->field($new_team, 'value')->textInput(['placeholder' => "Team value", 'type' => 'number', 'max' => 1000, 'min' => 1])->label(false) ?>
                    <?= \yii\helpers\Html::submitButton('Add team!', ['class' => 'btn btn-primary btn_position']) ?>
                </div>
            </li>
            <?php ActiveForm::end() ?>

            <?php $form = ActiveForm::begin(['action' => Url::to(['team/delete'])]); ?>
            <li><p class="team_delete team_add_hidden delete_league_hidden">Delete team</p>
                <div class="my_team_delete">
                    <h5>Delete team</h5>
                    <?= $form->field($new_team, 'id')->dropDownList($teamList)->label(false) ?>
                    <?= \yii\helpers\Html::submitButton('Delete team!', ['class' => 'btn btn-primary btn_position']) ?>
                </div>
            </li>
            <?php ActiveForm::end() ?>

            <?php $form = ActiveForm::begin(['action' => Url::to(['league/delete'])]); ?>
            <li><p class="league_delete team_add_hidden delete_team_hidden">Delete league</p>
                <div class="my_league_delete">
                    <h5>Czy na pewno chcesz usunąć ligę ?</h5>
                    <?= \yii\helpers\Html::activeHiddenInput($league, 'id') ?>
                    <?= \yii\helpers\Html::submitButton('Delete league!', [
                        'class' => 'btn btn-primary btn_position',
                        'data-confirm' => 'Are you sure you want to delete this item?',
                    ]) ?>
                </div>
            </li>
            <?php ActiveForm::end() ?>

        </ul>
    </div>
</aside>

<div class="container">
    <div><a href="<?= Url::to(['/']); ?>"><h3><-- Back</h3></a>
        <h1><?= \yii\helpers\Html::encode("{$league->name}") ?>: 19/20</h1></div>
    <?= Html::a('Create a match-day -->', ['match-day/add', 'id' => $league->id], ['class' => 'match-day']) ?>
    <?php if ($match_day_last): ?>
        <?= Html::a('Matchdays', ['match-day/matchdays', 'id' => $league->id, 'match_day_id' => $match_day_last->id], ['class' => 'matches']) ?>
    <?php endif; ?>
    <?= Html::a('Live table', ['match-day/table', 'id' => $league->id], ['class' => 'match-day match-day--table']) ?>
    <div class="content">
        <?= \yii\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'class' => 'team_table',
            'columns' => [
                'name',
                'coach',
                'stadium',
                'capacity',
                'town',
                'value'
            ],
            'summary' => false,
        ]) ?>
    </div>
</div>


