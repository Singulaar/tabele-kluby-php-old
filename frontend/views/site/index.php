<?php

/* @var $this yii\web\View */

/* @var $add_league League */

use frontend\models\League;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'My Yii Application';
?>
    <div class="newsletter">
        <ol>
            <?php sort($league) ?>
            <?php foreach ($league as $item): ?>
                <?php if ($item->user_id == 0): ?>
                    <li><?= \yii\helpers\Html::encode("{$item->name}") ?>
                        <ul>
                            <li>
                                <a href="<?= Url::to(['site/news']); ?>?id=<?= \yii\helpers\Html::encode("{$item->id}") ?>">News</a>
                            </li>
                            <li><a href="<?= Url::to(['']); ?>?id=<?= \yii\helpers\Html::encode("{$item->id}") ?>">Transfers</a>
                            </li>
                        </ul>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ol>
    </div>
<div class="container">
    <h1>Choose your league:</h1>
    <div class="content">
        <div class="row leagues">
            <?php sort($league) ?>
            <?php foreach ($league as $item): ?>
                <div class="col-4 league"><a
                            href="<?= Url::to(['/team/detail']); ?>?id=<?= \yii\helpers\Html::encode("{$item->id}") ?>"><img
                                src="../img/<?= $item->img ?>" alt="<?= \yii\helpers\Html::encode("{$item->name}") ?>">
                        <a/></div>
            <?php endforeach; ?>
        </div>
        <div class="row leagues">
            <div class="add_league col-6 offset-3">
                <div class="open"><p>Add own league !</p></div>
                <div class="add_form">
                    <?php $form = ActiveForm::begin(); ?>
                    <label>
                        <?= $form->field($add_league, 'name')->textInput(['placeholder' => "League name"])->label(false) ?>
                        <label>
                            <?= $form->field($add_league, 'img')->fileInput()->label(false) ?>
                        </label>
                        <?= \yii\helpers\Html::submitButton('Add league!', ['class' => 'btn btn-primary btn_position']) ?>
                        <?php \yii\widgets\ActiveForm::end() ?>
                        <p class="add_league_text">1 league per user!</p>
                </div>
            </div>
        </div>
    </div>
</div>
