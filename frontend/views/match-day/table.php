<?php
/* @var $this yii\web\View */
/* @var $league League */
/* @var $teams Team */

/* @var $matches Team */

use frontend\models\League;
use frontend\models\Team;
use yii\helpers\Html;


$league_id = $_GET['id'];
$this->title = 'My Yii Application - Live Table';
?>

<div class="container">
    <div>
        <div style="position: relative; top: -30px">
            <h3><?= Html::a('<-- Back', ['team/detail', 'id' => $league_id]) ?></h3>
            <h1>Live table</h1>
        </div>
        <div class="content">
            <table class="team_table">
                <tr>
                    <th>Team name</th>
                    <th>Points</th>
                    <th>Goals scored</th>
                    <th>Goals conceded</th>
                    <th>Balance</th>
                </tr>
                <?php foreach ($teams as $item): ?>
                    <tr>
                        <td><?= $item->name ?></td>
                        <td><?= $item->points ?></td>
                        <td><?= $item->goals_scored ?></td>
                        <td><?= $item->goals_conceded ?></td>
                        <td><?= $item->balance ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
