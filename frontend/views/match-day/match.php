<?php
/* @var $this yii\web\View */
/* @var $teams Team[] */
/* @var $match Match */
/* @var $match_day MatchDay */
/* @var $match_day_prev MatchDay */
/* @var $match_day_next MatchDay */
/* @var $teamList array */

/* @var $match_result Match[] */

use frontend\models\Match;
use frontend\models\MatchDay;
use frontend\models\Team;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$league_id = $_GET['id'];
$this->title = 'My Yii Application - Create Match';
?>

<div class="container">
    <div>
        <h1>Matchday: <?= Html::encode("{$match_day->number}") ?></h1>
        <?php if ($match_day_prev) : ?>
            <?= Html::a('<-- Prev match-day', ['match-day/matchdays', 'id' => $league_id, 'match_day_id' => $match_day_prev->id], ['class' => 'match-day match-day--prev']) ?>
            <br>
        <?php endif; ?>
        <div style="position: relative; top: -30px">
            <h3><?= Html::a('<-- Back', ['team/detail', 'id' => $league_id]) ?></h3>
            <?php if ($match_day_next) : ?>
                <?= Html::a('Next match-day -->', ['match-day/matchdays', 'id' => $league_id, 'match_day_id' => $match_day_next->id], ['class' => 'match-day match-day--next']) ?>
            <?php endif; ?>
        </div>
        <div class="content">
            <table class="create_match_table">
                <tr>
                    <th>Home team</th>
                    <th>Home goals</th>
                    <th>Visitor goals</th>
                    <th>Visitor team</th>
                </tr>
                <?php foreach ($match_result as $item): ?>
                    <tr class="table_border">
                        <td><?= $item->homeTeam->name ?></td>
                        <td><?= $item->home_score ?></td>
                        <td><?= $item->visitor_score ?></td>
                        <td><?= $item->visitorTeam->name ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
            <div class="create_match">
                <?php $form = ActiveForm::begin(); ?>
                <?= $form->field($match, 'home_team')->dropDownList($teamList)->label(false) ?>
                <?= $form->field($match, 'home_score')->textInput(['placeholder' => "goals", 'type' => 'number', 'class' => 'form-number', 'max' => 20, 'min' => 0, 'value' => 0])->label(false) ?>
                <?= $form->field($match, 'visitor_team')->dropDownList($teamList)->label(false) ?>
                <?= $form->field($match, 'visitor_score')->textInput(['placeholder' => "goals", 'type' => 'number', 'class' => 'form-number', 'max' => 20, 'min' => 0, 'value' => 0])->label(false) ?>
                <?= $form->field($match, 'date')->textInput(['type' => 'date'])->label(false) ?>
                <?= $form->field($match, 'match_day_id')->hiddenInput(['value' => $match_day->id])->label(false) ?>
                <?= \yii\helpers\Html::submitButton('Add match!', ['class' => 'btn btn-primary btn_position']) ?>
                <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>