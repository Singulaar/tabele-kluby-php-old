<?php
/* @var $this yii\web\View */

/* @var $match_day MatchDay */

use frontend\models\MatchDay;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$league_id = $_GET['id'];
$this->title = 'My Yii Application - Match Day';
?>
<body class="background_matchdays">
<div class="container">
    <div><h3><?= Html::a('<-- Back', ['team/detail', 'id' => $league_id]) ?></h3>
        <div class="content">
            <?php $form = ActiveForm::begin(); ?>
            <div class="match">
                <h3>Add matchday</h3>
                <?= $form->field($match_day, 'number')->textInput(['placeholder' => "Matchday number", 'type' => 'number', 'max' => 36, 'min' => 1])->label(false) ?>
                <br>
                <?= \yii\helpers\Html::submitButton('Add match-day!', ['class' => 'btn btn-primary btn_position']) ?>
            </div>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</body>