<?php

namespace frontend\controllers;

use frontend\models\League;
use frontend\models\Match;
use frontend\models\MatchDay;
use frontend\models\Team;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


/**
 * MatchDay controller
 */
class MatchDayController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
//                'only' => ['match_day'],
                'rules' => [
                    [
                        'actions' => ['add', 'matchdays', 'table'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    public function actionAdd($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $match_day = new MatchDay();
            $match_day_last = MatchDay::find()->where(['league_id' => $id])->orderBy('number DESC')->one();
            $match_day->league_id = $id;
            if ($match_day->load(Yii::$app->request->post()) && $match_day->validate()) {
                $match_number = MatchDay::find()->where(['number' => $match_day, 'league_id' => $id])->one();
                if ($match_number) {
                    $num = $match_number->number;
                } else {
                    $num = null;
                }
                if ($match_day->number == $num) {
                    return $this->render('match-day', [
                        'match_day' => $match_day
                    ]);
                } else {
                    $match_day->save();
                    if($match_day_last) {
                        return $this->redirect(['match-day/matchdays', 'id' => $id, 'match_day_id' => $match_day_last->id]);
                    } else {
                        return $this->redirect(['team/detail', 'id' =>$id]);
                    }
                }
            }
            return $this->render('match-day', [
                'match_day' => $match_day,
            ]);
        }
    }

    public function actionMatchdays($id, $match_day_id)
    {
        $match = new Match();

        $match_day = MatchDay::find()->where(['league_id' => $id, 'id' => $match_day_id])->orderBy('number DESC')->one();
        $match_result = Match::find()->where(['match_day_id' => $match_day->id])->orderBy('date')->all();
        $match_day_prev = MatchDay::find()->where(['league_id' => $id])->andWhere(['<', 'number', $match_day->number])->orderBy('number DESC')->one();
        $match_day_next = MatchDay::find()->where(['league_id' => $id])->andWhere(['>', 'number', $match_day->number])->orderBy('number ASC')->one();

        $teamsQuery = Team::find()->where(['league_id' => $id])->orderBy('league_id');
        foreach ($match_result as $match_result_one) {
            $teamsQuery->andWhere(['!=', 'id', $match_result_one->home_team]);
            $teamsQuery->andWhere(['!=', 'id', $match_result_one->visitor_team]);
        }
        $teams = $teamsQuery->all();
        $teamList = ArrayHelper::map($teams, 'id', 'name');

        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if ($match->load(Yii::$app->request->post()) && $match->validate()) {
            $result = $match->home_score <=> $match->visitor_score;
            switch ($result) {
                case 1:
                    $match->home_team_points = 3;
                    $match->visitor_team_points = 0;
                    break;
                case 0:
                    $match->home_team_points = 1;
                    $match->visitor_team_points = 1;
                    break;
                case -1:
                    $match->home_team_points = 0;
                    $match->visitor_team_points = 3;
                    break;
            }
            $match->save();
            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->render('match', [
            'teamList' => $teamList,
            'teams' => $teams,
            'match' => $match,
            'match_day' => $match_day,
            'match_result' => $match_result,
            'match_day_prev' => $match_day_prev,
            'match_day_next' => $match_day_next
        ]);
    }

    public function actionTable($id)
    {

        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $teams = Team::find()->where(['league_id' => $id])->all();
        $teamList = ArrayHelper::map($teams, 'id', 'name');

        foreach ($teams as $team) {
            $points = 0;
            $goals_scored = 0;
            $goals_conceded = 0;
            $result = Match::find()->where(['home_team' => $team->id])->orWhere(['visitor_team' => $team->id])->all();


            foreach ($result as $match_result) {
                if ($match_result->home_team == $team->id) {
                    $points += $match_result->home_team_points;
                }
                if ($match_result->visitor_team == $team->id) {
                    $points += $match_result->visitor_team_points;
                }
            }
            foreach ($result as $goal_scr) {
                if ($goal_scr->home_team == $team->id) {
                    $goals_scored += $goal_scr->home_score;
                }
                if ($goal_scr->visitor_team == $team->id) {
                    $goals_scored += $goal_scr->visitor_score;
                }
            }
            foreach ($result as $goal_cnc) {
                if ($goal_cnc->home_team == $team->id) {
                    $goals_conceded += $goal_cnc->visitor_score;
                }
                if ($goal_cnc->visitor_team == $team->id) {
                    $goals_conceded += $goal_scr->home_score;
                }
            }
            $team->points = $points;
            $team->goals_scored = $goals_scored;
            $team->goals_conceded = $goals_conceded;
            $balance = $goals_scored - $goals_conceded;
            $team->balance = $balance;
        }
        arsort($teams);

        return $this->render('table', [
            'teamList' => $teamList,
            'teams' => $teams,
        ]);
    }
}

