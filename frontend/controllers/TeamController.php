<?php

namespace frontend\controllers;

use frontend\models\League;
use frontend\models\Match;
use frontend\models\MatchDay;
use frontend\models\Team;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


/**
 * Team controller
 */
class TeamController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
//                'only' => ['team'],
                'rules' => [
                    [
                        'actions' => ['detail', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    public function actionDetail($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $team = Team::find()->where(['league_id' => $id])->orderBy('league_id');
        $teams = Team::find()->where(['league_id' => $id])->all();
        $league = League::find()->where(['id' => $id])->orderBy('id')->one();
        $user_id = Yii::$app->user->id;

        $match_day_last = MatchDay::find()->where(['league_id' => $id])->orderBy('number DESC')->one();

        $new_team = new Team;
        if ($new_team->load(Yii::$app->request->post()) && $new_team->validate()) {
            $new_team->league_id = $id;
            $new_team->save();
            return $this->refresh();
        }

        $teamList = ArrayHelper::map($teams, 'id', 'name');

        $dataProvider = new ActiveDataProvider(['query' => $team]);

        if ($id == $user_id) {
            return $this->render('myteam',
                [
                    'team' => $team,
                    'teamList' => $teamList,
                    'league' => $league,
                    'new_team' => $new_team,
                    'match_day_last' => $match_day_last,
                    'dataProvider' => $dataProvider
                ]);
        }
        return $this->render('detail', [
                'team' => $team,
                'league' => $league,
                'match_day_last' => $match_day_last,
                'dataProvider' => $dataProvider,
            ]
        );
    }

    public function actionDelete()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $team_id = Yii::$app->request->post('Team');

        $delete_team = Team::find()->where(['id' => $team_id])->one();

        if ($delete_team) {

            $league_id = $delete_team->league_id;
            $delete_match = Match::find()->where(['home_team' => $delete_team->id])->orWhere(['visitor_team' => $delete_team->id])->all();

            foreach ($delete_match as $match) {
                $match->delete();
            }
            $delete_team->delete();

            return $this->redirect(['team/detail', 'id' => $league_id]);
        }
        return $this->goBack();
    }
}

