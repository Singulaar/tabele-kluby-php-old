<?php

namespace frontend\controllers;

use frontend\models\League;
use frontend\models\Match;
use frontend\models\MatchDay;
use frontend\models\Team;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * League controller
 */
class LeagueController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
//                'only' => ['league'],
                'rules' => [
                    [
                        'actions' => ['add', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return Response
     */
    public function actionAdd()
    {
        $add_league = new League();
        $id = Yii::$app->user->id;
        $user_league = League::find()->where(['user_id' => $id])->all();


        if ($user_league === $id) {
            return $this->redirect(Yii::$app->request->referrer);
        }
        if ($add_league->load(Yii::$app->request->post())) {
            $add_league->img = UploadedFile::getInstance($add_league, 'img');
            $add_league->user_id = Yii::$app->user->id;
            $add_league->id = Yii::$app->user->id;


            if ($add_league->upload()) {
                $add_league->save();
            }
        }
        return $this->goBack();
    }


    public function actionDelete()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $league_id = Yii::$app->request->post('League')['id'];
        $delete_league = League::find()->where(['id' => $league_id])->one();
        $delete_team = Team::find()->where(['league_id' => $league_id])->all();
        $delete_matchday = MatchDay::find()->where(['league_id' => $league_id])->all();

        foreach ($delete_team as $team) {
            $team->delete();
            $delete_match = Match::find()->where(['home_team' => $team->id])->orWhere(['visitor_team' => $team->id])->all();
            foreach ($delete_match as $match) {
                $match->delete();
            }
        }

        foreach ($delete_matchday as $matchday) {
            $matchday->delete();
        }
        $delete_league->delete();
        return $this->goBack();
    }
}

