<?php

namespace frontend\controllers;

use frontend\models\Team;
use common\models\User;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;


/**
 * Site controller
 */
class UserMenuController extends Controller
{
    private $email;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
//                'only' => ['menu', 'change-password', 'change-email', 'personal-data'],
                'rules' => [
                    [
                        'actions' => ['menu', 'change-password', 'change-email', 'personal-data'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionMenu()
    {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $this->layout = 'user-menu';
        $user = Yii::$app->user->identity;
        $newFavouriteClub = Team::find()->where(['id' => $user->favouriteClub])->one();

        if ($user->username === 'Admin') {
            $this->layout = 'admin-menu';
        }


        return $this->render('menu-home', [
            'user' => $user,
            'newFavouriteClub' => $newFavouriteClub,
        ]);
    }

    public function actionChangePassword()
    {
        /** @var User $user */
        $this->layout = 'user-menu';

        $user = Yii::$app->user->identity;
        if ($user->username === 'Admin') {
            $this->layout = 'admin-menu';
        }
        $user->scenario = User::SCENARIO_CHANGE_PASSWORD;

        if ($user->load(Yii::$app->request->post()) && $user->validate()) {
            $user->setPassword($user->newPassword);
            $user->save();
            return $this->render('change-password-confirm');
        }

        return $this->render('change-password', [
            'user' => $user,
        ]);
    }

    public function actionPersonalData()
    {
        /** @var User $user */

        $this->layout = 'user-menu';

        $user = Yii::$app->user->identity;
//        var_dump($newFavouriteClub); die;
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        if ($user->username === 'Admin') {
            $this->layout = 'admin-menu';
        }



        $user->scenario = User::SCENARIO_CHANGE_PERSONAL_DATA;

        $teams = Team::find()->all();
        $teamsList = ArrayHelper::map($teams, 'id', 'name');
        if ($user->load(Yii::$app->request->post()) && $user->validate()) {
            $user->name = $user->newName;
            $user->surname = $user->newSurname;
            $user->city = $user->newCity;
            $user->favouriteClub = $user->newFavouriteClub;
            $user->save();
            return $this->render('change-personal-data-confirm');
        }

        return $this->render('personal-data', [
            'user' => $user,
            'teams' => $teams,
            'teamsList' => $teamsList,
        ]);
    }

    public function actionChangeEmail()
    {
        /** @var User $user */
        $this->layout = 'user-menu';

        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $user = Yii::$app->user->identity;
        if ($user->username === 'Admin') {
            $this->layout = 'admin-menu';
        }
        $user->scenario = User::SCENARIO_CHANGE_EMAIL;
        if ($user->load(Yii::$app->request->post()) && $user->validate()) {
            $user->email = $user->newEmail;
            $user->save();
            return $this->render('change-email-confirm');
        }

        return $this->render('change-email', [
            'user' => $user,
        ]);
    }
}